.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/mbsim-hw.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/mbsim-hw
    .. image:: https://readthedocs.org/projects/mbsim-hw/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://mbsim-hw.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/mbsim-hw/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/mbsim-hw
    .. image:: https://img.shields.io/pypi/v/mbsim-hw.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/mbsim-hw/
    .. image:: https://img.shields.io/conda/vn/conda-forge/mbsim-hw.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/mbsim-hw
    .. image:: https://pepy.tech/badge/mbsim-hw/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/mbsim-hw
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/mbsim-hw

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

.. image:: https://gitlab.com/nee2c/mbsim-hw/badges/master/pipeline.svg

.. image:: https://readthedocs.org/projects/mbsim-hw/badge/?version=latest
    :target: https://mbsim-hw.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

|

==========
mbsim-hw
==========

An example of an addon for mbsim

Installation
============


use pip ``pip install <url repo>``


Usage
=====

.. code-block::

    mbsim hw {tcp,udp,rtu} [OPTIONS] strings ...

Main Package
============

The command line utils can be found in the package `mbsim <https://gitlab.com/nee2c/mbsim>`_.
This package will have utils for modbus server and clients.

.. _pyscaffold-notes:

Making Changes & Contributing
=============================

This project uses `pre-commit`_, please make sure to install it before making any
changes::

    pip install pre-commit
    cd mbsim-hw
    pre-commit install

It is a good idea to update the hooks to the latest version::

    pre-commit autoupdate

Don't forget to tell your contributors to also install and use pre-commit.

.. _pre-commit: https://pre-commit.com/

Note
====

This project has been set up using PyScaffold 4.1.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
