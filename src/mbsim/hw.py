import mbsim.utils
import mbsim.core.server as server

def getargs(parser):
    """
    Add encoded string to imput register
    """
    args = parser(name='hw', help='Hellow World')
    parsers = mbsim.utils.protocalargs(args)

    for argparser in ("tcp", "udp", "rtu"):
        parsers[argparser].add_argument('string', type=str, default=["Hello", "World"], help='String to encode', nargs='*')
    
    return run

def run(args):
    """
    Encode string and write to input register
    """
    context = server.genContext()
    context[0].setValues(4, 0, list(bytearray(" ".join(args.string).encode())))
    server.start(args.protocol, address=(args.address, args.port), context=context)